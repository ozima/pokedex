import { Component, OnInit } from '@angular/core';
import { PokemonService } from '../services/pokemon.service';
import { ActivatedRoute } from '@angular/router';
import { FindValueSubscriber } from 'rxjs/internal/operators/find';

@Component({
  selector: 'app-details',
  templateUrl: './details.page.html',
  styleUrls: ['./details.page.scss'],
})
export class DetailsPage implements OnInit {
  details: any;

  //settings for the ionic-slides component which is used to show 
  //the sprites of the target pokemon
  slideOpts = {
    autoplay: {
      delay: 2000,
      disableOnInteraction: true
    }
  };

  constructor(private pokeService: PokemonService, private route: ActivatedRoute) { }

  //get details for the pokemon
  ngOnInit() {
    let index = this.route.snapshot.paramMap.get('index');
    this.pokeService.getPokeDetails(index).subscribe(details => {
      this.details = details;
    });
  }

}
