import { Component, OnInit, ViewChild } from '@angular/core';
import { PokemonService } from '../services/pokemon.service';
import { IonInfiniteScroll } from '@ionic/angular';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {
  offset = 0;
  pokemon = [];

  //add the infinite scrolling feature to the pokedex
  @ViewChild(IonInfiniteScroll,  {static: false}) infinite: IonInfiniteScroll;

  constructor(private pokeService: PokemonService) {}

  ngOnInit() {
    this.loadPokemon();
  }

  loadPokemon(loadMore = false, event?) {
    //bumps the offset up to load more more pokemon
    if (loadMore) {
      this.offset += 25;
    }

    this.pokeService.getPokemon(this.offset).subscribe(res => {
      //the spread operator ... is used to fill the array with two other arrays
      this.pokemon = [...this.pokemon, ...res];
  
      if (event) {
        event.target.complete();
      }
  
      //uncomment if you only want the original 150 pokemon
      //if (this.offset == 125) {
      //  this.infinite.disabled = true;
      //}
    });
  }

  onSearchChange(e) {
    let value = e.detail.value;

    if (value == '') {
      this.offset = 0;
      this.loadPokemon();
      return;
    }

    this.pokeService.findPokemon(value).subscribe(res => {
      this.pokemon = [res];
    }, err => {
      this.pokemon = [];
    });
  }
}
