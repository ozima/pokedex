import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})

export class PokemonService {
  apiUrl = 'https://pokeapi.co/api/v2';
  imgUrl = 'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/';

  constructor(private http: HttpClient) { }

  getPokemon(offset = 0) { //the offset should help with not loading all the data
    return this.http.get(`${this.apiUrl}/pokemon?offset=${offset}&limit=25`)
    .pipe(map(result => {
      return result['results'];
    }),
    map(pokemon => {
      return pokemon.map((poke, index) => {
        poke.image=this.getPokeImg(offset + index + 1);
        poke.pokeIndex = offset + index + 1;
        return poke;
      });
    }));
  }

  //find target pokemon
  findPokemon(search) {
    return this.http.get(`${this.apiUrl}/pokemon/${search}`)
    .pipe(map(pokemon => {
      pokemon['image'] = this.getPokeImg(pokemon['id']);
      pokemon['pokeIndex'] = pokemon['id'];
      return pokemon;
    }));
  }

  //find the image of target pokemon
  getPokeImg(index) {
    return `${this.imgUrl}${index}.png`;
  }

  //get the details and sprites of target pokemon
  getPokeDetails(index) {
    return this.http.get(`${this.apiUrl}/pokemon/${index}`)
    .pipe(map(poke => {
      let sprites = Object.keys(poke['sprites']);
      poke['images'] = sprites.map(spriteKey => poke['sprites'][spriteKey])
      .filter(img => img);
      return poke;
    }));
  }
}
